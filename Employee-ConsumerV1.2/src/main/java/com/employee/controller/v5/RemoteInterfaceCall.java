package com.employee.controller.v5;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.employee.Model.Employee;

@FeignClient("Employee-Zuul-Service")
public interface RemoteInterfaceCall {

	@RequestMapping(method = RequestMethod.GET,value = "/employee/employeeDtls")
	public Employee getEmployeeDetails();
	
}
