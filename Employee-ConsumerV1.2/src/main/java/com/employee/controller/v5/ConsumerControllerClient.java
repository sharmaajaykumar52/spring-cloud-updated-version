package com.employee.controller.v5;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestClientException;

import com.employee.Model.Employee;

public class ConsumerControllerClient {

	@Autowired
	RemoteInterfaceCall remoteInterfaceCall;

	
	public void getEmployee() throws RestClientException, IOException {
		Employee emp = remoteInterfaceCall.getEmployeeDetails();
		System.out.println("Employee Details from Zuul Proxy Is  are " + emp.toString());
	}

}
