package com.employee.controller.v4;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.employee.Model.Employee;

@FeignClient("Employee-Producer")
public interface RemoteCallService {

	@RequestMapping(method=RequestMethod.GET, value="/employee/employeeDtls")
	public Employee getData();
	
}
