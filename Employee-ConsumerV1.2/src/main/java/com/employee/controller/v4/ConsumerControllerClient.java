package com.employee.controller.v4;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestClientException;

import com.employee.Model.Employee;

public class ConsumerControllerClient {

	@Autowired
	private RemoteCallService remoteCallService;

	public void getEmployee() throws RestClientException, IOException {
		Employee emp = remoteCallService.getData();
		System.out.println("Employee Details are " + emp.toString());
	}

}
