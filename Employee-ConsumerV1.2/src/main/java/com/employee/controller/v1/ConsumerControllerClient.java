package com.employee.controller.v1;

import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class ConsumerControllerClient {

	public void getEmployee() throws RestClientException, IOException {
		String employeeDetailsUrl = "http://localhost:8085/employee/employeeDtls";
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = null;
		try {
			response = restTemplate.exchange(employeeDetailsUrl, HttpMethod.GET, getHeaders(), String.class);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		System.out.println("Employee Details are " + response.getBody());
	}

	public void getOrganizationDetails() {
		String organizationDtlsUrl = "http://localhost:8085/employee/organization";
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> restResponse = null;
		try {
			restResponse = restTemplate.exchange(organizationDtlsUrl, HttpMethod.GET, getHeaders(), String.class);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		System.out.println("Organization Details are  " + restResponse.getBody());
	}

	private static HttpEntity<?> getHeaders() throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		return new HttpEntity<>(headers);
	}
}
