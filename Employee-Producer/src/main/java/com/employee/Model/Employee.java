package com.employee.Model;

import java.io.Serializable;
import java.util.List;

public class Employee implements Serializable {

	private String employeeId;
	private String employeeName;
	private String employeeAge;
	private String contactNumber;
	private List<OrganizationDetails> organizationDtls;
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeAge() {
		return employeeAge;
	}
	public void setEmployeeAge(String employeeAge) {
		this.employeeAge = employeeAge;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public List<OrganizationDetails> getOrganizationDtls() {
		return organizationDtls;
	}
	public void setOrganizationDtls(List<OrganizationDetails> organizationDtls) {
		this.organizationDtls = organizationDtls;
	}

}
