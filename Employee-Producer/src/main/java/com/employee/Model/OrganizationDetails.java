package com.employee.Model;

import java.io.Serializable;

public class OrganizationDetails implements Serializable {

	private String organizationId;
	private String organizationName;
	private String organizationLocation;
	public String getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getOrganizationLocation() {
		return organizationLocation;
	}
	public void setOrganizationLocation(String organizationLocation) {
		this.organizationLocation = organizationLocation;
	}
	
}
