package com.employee.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.employee.Model.Employee;
import com.employee.Model.OrganizationDetails;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class EmployeeController {

	Employee emp = null;
	List<OrganizationDetails> organizationList = null;

	EmployeeController() {
		OrganizationDetails orgDtls1 = new OrganizationDetails();
		OrganizationDetails orgDtls2 = new OrganizationDetails();
		emp = new Employee();
		organizationList = new ArrayList<>();
		orgDtls1.setOrganizationId("111");
		orgDtls1.setOrganizationName("Infogain");
		orgDtls1.setOrganizationLocation("Noida");
		orgDtls2.setOrganizationId("112");
		orgDtls2.setOrganizationLocation("Amritsar");
		orgDtls2.setOrganizationName("GNDU");
		organizationList.add(orgDtls1);
		organizationList.add(orgDtls2);
		emp.setContactNumber("9599817440");
		emp.setEmployeeAge("22");
		emp.setEmployeeId("103201");
		emp.setEmployeeName("Harry");
		emp.setOrganizationDtls(organizationList);

	}

	@RequestMapping(value = "/employeeDtls", method = RequestMethod.GET)
	@HystrixCommand(fallbackMethod = "getDataFallBack")
	public Employee getEmployeeDetails() {
		System.out.println("Displaying the employee Details.........");
		//throw new NullPointerException();
		return emp;
	}

	@GetMapping(value = "/organization")
	public List<OrganizationDetails> getOrganizationDetails() {
		System.out.println("Displaying the Organizaton details .......");
		return organizationList;
	}

	public Employee getDataFallBack() {

		Employee emp = new Employee();
		emp.setContactNumber("EMERGENCY-CONTACT-NUMBER");
		emp.setEmployeeAge("AGE");
		emp.setEmployeeId("101010");
		emp.setEmployeeName("FALL-BACK-METHOD");
		emp.setOrganizationDtls(new ArrayList<>());

		return emp;

	}

}
