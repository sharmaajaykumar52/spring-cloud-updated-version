package com.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AvailabilityFilteringRule;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;

public class RibbonConfiguration {

	@Autowired
	IClientConfig config;

	@Bean
	public IPing ribbonPing(IClientConfig config) {
		System.out.println("$$$$  Inside the ribbonPing method  $$$$$");
		System.out.println("@@@@@   Client Name    " + config.getClientName() + " Class Name   " + config.getClass()
				+ " NameSpace " + config.getNameSpace());
		return new PingUrl();
	}

	@Bean
	public IRule ribbonRule(IClientConfig config) {
		System.out.println("#### Inside the ribbonRule Method  #####");
		return new AvailabilityFilteringRule();
	}
}
