package com.employee.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.employee.Model.Employee;
import com.employee.Model.OrganizationDetails;


@RestController
@Profile("english")
public class EmployeeControllerEnglish {

	Employee emp = null;
	List<OrganizationDetails> organizationList = null;

	EmployeeControllerEnglish() {
		System.out.println("English .........................");
		OrganizationDetails orgDtls1 = new OrganizationDetails();
		OrganizationDetails orgDtls2 = new OrganizationDetails();
		emp = new Employee();
		organizationList = new ArrayList<>();
		orgDtls1.setOrganizationId("111");
		orgDtls1.setOrganizationName("Infogain");
		orgDtls1.setOrganizationLocation("Noida");
		orgDtls2.setOrganizationId("112");
		orgDtls2.setOrganizationLocation("Amritsar");
		orgDtls2.setOrganizationName("GNDU");
		organizationList.add(orgDtls1);
		organizationList.add(orgDtls2);
		emp.setContactNumber("9599817440");
		emp.setEmployeeAge("22");
		emp.setEmployeeId("103201");
		emp.setEmployeeName("Harry");
		emp.setOrganizationDtls(organizationList);

	}

	@RequestMapping(value = "/EmployeeDtlsInEnglish", method = RequestMethod.GET)
	public Employee getEmployeeDetails() {
		System.out.println("Displaying the employee Details in english.........");
		// throw new NullPointerException();
		return emp;
	}

}
