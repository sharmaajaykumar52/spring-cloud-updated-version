package com.employee.controller.v2;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.cloud.client.discovery.DiscoveryClient;

public class ConsumerControllerClient {

	@Autowired
	DiscoveryClient discoveryClient;

	public void getEmployee() throws RestClientException, IOException {
		String employeeDetailsUrl = getBaseUriFromDiscoveryClient() + "/employee/employeeDtls";
		System.out.println(employeeDetailsUrl);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = null;
		try {
			response = restTemplate.exchange(employeeDetailsUrl, HttpMethod.GET, getHeaders(), String.class);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		System.out.println("Employee Details are " + response.getBody());
	}

	public void getOrganizationDetails() {
		String organizationDetailsIrl = getBaseUriFromDiscoveryClient() + "/employee/organization";
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> restResponse = null;
		try {
			restResponse = restTemplate.exchange(organizationDetailsIrl, HttpMethod.GET, getHeaders(), String.class);
		} catch (Exception e) {
			System.out.println("Error Occured " + e.getMessage());
		}
		System.out.println("Organization Details are  " + restResponse.getBody());
	}

	private String getBaseUriFromDiscoveryClient() {
		List<ServiceInstance> serviceInstanceList = discoveryClient.getInstances("Employee-ConsumerV1.1");
		String baseUrl = "";
		for (ServiceInstance serviceInstance : serviceInstanceList) {
			baseUrl = serviceInstance.getUri().toString();
		}
		return baseUrl;
	}

	private static HttpEntity<?> getHeaders() throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		return new HttpEntity<>(headers);
	}
}
